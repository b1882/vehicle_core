import cv2
from vehicle_core import ICartWheelZoneDetect
import time
import os


def addRectangle(im, boxes):
    font = cv2.FONT_HERSHEY_SIMPLEX
    for box in boxes:
        zone = box["zone"]
        cv2.rectangle(im, (zone[0], zone[1]),
                      (zone[2], zone[3]), (0, 0, 255), 2)
        cv2.putText(im, "%s:%.2f" % (
            box["cls"], box["score"]), (zone[0], zone[1]), font, 1, (0, 255, 0))


def run():
    model_file = "/home/zqp/gitlab/models"
    detector = ICartWheelZoneDetect(model_file)

    pic_dir = "/home/zqp/testpic/cartwheel/"

    cv2.namedWindow("img", 0)
    for picname in sorted(os.listdir(pic_dir)):
        pic_path = pic_dir+picname
        img = cv2.imread(pic_path)

        start = time.time()
        boxes = detector.detect(img)
        end = time.time()
        print("detect cost time: %s ms" % ((end-start)*1000))
        addRectangle(img, boxes)
        cv2.imshow("img", img)
        cv2.waitKey(0)


if __name__ == "__main__":
    run()
