#coding=utf-8
import os
import cv2
import time
import numpy as np

from vehicle_core import IVehicleZoneDetect
from vehicle_core import IVehiclePostureClassifier
from vehicle_core import IVehicleColorClassifier
from vehicle_core import IVehicleHeadClassifier
from vehicle_core import IVehicleTailClassifier
from vehicle_core import IVehicleStructure
from vehicle_core import IPlateRecognize

class vehicleMaster:
    def __init__(self,model_dir,gpu_id,color=True,type=True,struct=True):
        model_dir = model_dir + "/" if not model_dir.endswith("/") else model_dir
        self.__net_vehicle = IVehicleZoneDetect(model_dir,gpu_id)
        self.__gpu_id = gpu_id
        self.__color = color
        self.__type = type
        self.__struct = struct

        self.__net_plate = IPlateRecognize(model_dir)

        if self.__color:
            self.__net_color = IVehicleColorClassifier(model_dir,gpu_id)

        if self.__type:
            self.__net_posture = IVehiclePostureClassifier(model_dir,gpu_id)
            self.__net_head = IVehicleHeadClassifier(model_dir,gpu_id)
            self.__net_tail = IVehicleTailClassifier(model_dir,gpu_id)

        if self.__struct:
            self.__net_struct = IVehicleStructure(model_dir,gpu_id)

    def __getDetectStruct(self):
        result = {}
        result['vehicleZone'] = None
        result['vehicleColor'] = None
        result['vehiclePosture'] = None
        result['vehicleType'] = None
        result['vehiclePlateLicense'] = None
        result['vehicleStruct'] = None

        return result

    def detect(self,img_bgr: np.array):
        results = []
        boxes = self.__net_vehicle.detect(img_bgr)

        for zone in boxes:
            result = self.__getDetectStruct()
            result['vehicleZone'] = zone
            im_roi = img_bgr[zone[1]:zone[3],zone[0]:zone[2]]
            result['vehiclePlateLicense'] = self.__net_plate.recognize(im_roi.copy())

            if self.__color:
                color_result = self.__net_color.classify(im_roi)
                result['vehicleColor'] = color_result

            if self.__type:
                posture_result = self.__net_posture.classify(im_roi)
                result['vehiclePosture'] = posture_result

                if posture_result['category']=="车头":
                    head_result = self.__net_head.classify(im_roi)
                    result['vehicleType'] = head_result

                    if self.__struct:
                        cv2.imwrite("./tmp.jpg", im_roi)
                        struct_result = self.__net_struct.detect("./tmp.jpg")
                        result['vehicleStruct'] = struct_result
                elif posture_result['category']=="车尾":
                    tail_result = self.__net_tail.classify(im_roi)
                    result['vehicleType'] = tail_result
            results.append(result) 
        return results

if __name__=="__main__":
    model_dir="/root/models/vehicle"
    detector = vehicleMaster(model_dir, 0, True, True, False) 
    pic_dir = "/root/testpic/"

    for pic_name in os.listdir(pic_dir):
        print(pic_dir+pic_name)
        im = cv2.imread(pic_dir+pic_name)
        result = detector.detect(im)
        print(result)

