#coding=utf-8
import cv2
import os
from .IObjZoneDetect import IObjZoneYOLOV3Detect
import numpy as np

class IVehicleStructure:
    def __init__(self,model_dir,gpu_id):
        model_dir = model_dir + '/' if not model_dir.endswith('/') else model_dir

        window_cfg = model_dir + 'vehicle/vehiclewindows.cfg'
        window_weights = model_dir + 'vehicle/vehiclewindows.weights'
        window_name_file = model_dir + "vehicle/vehiclewindows.names"
        self.__window_names = [name.strip() for name in open(window_name_file).readlines()]
        self.__window_detector = IObjZoneYOLOV3Detect(window_cfg, window_weights, len(self.__window_names), gpu_id)

        label_cfg = model_dir + 'vehicle/vehiclewindowslabel.cfg'
        label_weights = model_dir + 'vehicle/vehiclewindowslabel.weights'
        label_name_file = model_dir + "vehicle/vehiclewindowslabel.names"
        self.__label_names = [name.strip() for name in open(label_name_file).readlines()]
        self.__label_detector = IObjZoneYOLOV3Detect(label_cfg, label_weights, len(self.__label_names), gpu_id)

        self.__gpu_id = gpu_id
        self.__confidence = 0.65

    def detect(self,im_path: str, confidence=0.7):
        result = {'biaoqian': [], 'guajian': [], 'baijian': [], 'zheyangban': [], 'fujiashi': [], 'siji': []}
        window_result = self.__window_detector.detect(im_path, confidence)
        if not len(window_result):
            return result
        sorted(window_result, key = lambda a: a["score"], reverse=True)

        zone = window_result[0]["zone"]
        im = cv2.imread(im_path)
        roi = im[zone[1]:zone[3], zone[0]:zone[2]]
        cv2.imwrite("tmp.jpg", roi)

        label_result = self.__label_detector.detect("tmp.jpg", confidence)

        for label in label_result:
            zone1 = label["zone"]
            zone1 = [zone1[0]+zone[0], zone1[1]+zone[1], zone1[2]+zone[0], zone1[3]+zone[1]]
            result[self.__label_names[label["cls"]]].append(zone1 + [label["score"]])

        return result

def addRectangle(im: np.array, result: list, color: tuple):
    for r in result:
        cv2.rectangle(im, (r[0], r[1]), (r[2], r[3]), color, 2)


def addRectangle1(im: np.array,result: dict):
    addRectangle(im, result["biaoqian"], (0,0,255))
    addRectangle(im, result["guajian"], (255,0,0))
    addRectangle(im, result["baijian"], (0,255,0))
    addRectangle(im, result["zheyangban"], (255,255,0))
    addRectangle(im, result["fujiashi"], (255,0,255))
    addRectangle(im, result["siji"], (0,255,255))

def run():
    model_dir=r'/home/zqp/gitlab/models'
    gpu_id = 0
    detector = IVehicleStructure(model_dir,gpu_id)

    picDir = r'/home/zqp/testpic/vehicle1/'
    for picName in os.listdir(picDir):
        im = cv2.imread(picDir+picName)
        result = detector.detect(picDir+picName)
        addRectangle1(im,result)
        cv2.imshow('im',im)
        if cv2.waitKey(0)==27:
            break
    
if __name__=="__main__":
    run()
