import cv2
import os
# from objTypeClassifier import ObjTypeClassifier
import faiss
import numpy as np
import torch
import pickle
from .re_ranking import re_rank
from .vehicleTypeHead import IVehicleHeadClassifier

class IVehicleRetrieval:
    def __init__(self, model_dir, gpu_id=0):
        self.__extracter = IVehicleHeadClassifier(model_dir, gpu_id)
        self.__feature_len = 2048

    @staticmethod
    def loadRetrievalModel(retrieval_model_path):
        assert (os.path.exists(retrieval_model_path))

        retrieval_model = faiss.read_index(retrieval_model_path)
        return retrieval_model

    def extractFeature(self,im_bgr):
        try:
            feature = self.__extracter.extractFeature(im_bgr)
        except:
            return []

        return feature

    def buildRetrievalDatabase(self,features,retrieval_model_path):
        if os.path.exists(retrieval_model_path):
            retrieval_model = faiss.read_index(retrieval_model_path)
        else:
            retrieval_model = faiss.IndexFlatL2(self.__feature_len)

        features = np.array(features, dtype=np.float32)
        retrieval_model.add(features)
        faiss.write_index(retrieval_model,retrieval_model_path)

        return retrieval_model

    def query(self,im, k, retrieval_model):
        feature = self.extractFeature(im)

        feature = np.array([feature], dtype=np.float32)
        res = retrieval_model.search(feature,k)
        return res[1][0], 1-res[0][0]/2

    @staticmethod
    def secondQuery(query_feature, gallery_features):
        q = torch.from_numpy(np.array([query_feature]))
        g = torch.from_numpy(gallery_features)
        distance = re_rank(q, g).flatten()
        distance_index = distance.argsort()
        distance = np.sort(distance)

        return distance_index, 1 - distance

def buildRetrievalDatabase():
    model_dir = "/home/zqp/gitlab/models"
    net = IVehicleRetrieval(model_dir, 0)

    features = []
    index = 0
    pic_dir = "/home/zqp/testpic/vehicle1/"

    pic_paths = sorted([pic_dir+pic_name for pic_name in os.listdir(pic_dir) if pic_name.endswith(".jpg")])
    num = len(os.listdir(pic_dir))

    for pic_path in pic_paths:
        im = cv2.imread(pic_path)
        res = net.extractFeature(im)

        if len(res):
            features.append(res)

        index += 1
        print ("processed***************%s/%s"%(index,num))

    net.buildRetrievalDatabase(features,"./123.index")

def query():
    model_dir = "/home/zqp/gitlab/models"
    retrieval_model_path="./123.index"
    net = IVehicleRetrieval(model_dir)
    retrieval_model = net.loadRetrievalModel(retrieval_model_path)
    features = np.array(pickle.load(open("features.pkl", "rb")))

    pic_dir = "/home/zqp/testpic/patentRetrieval/"
    pic_paths = sorted([pic_dir+pic_name for pic_name in os.listdir(pic_dir) if pic_name.endswith(".jpg")])
    for pic_name in os.listdir(pic_dir):
        print(pic_name)
        im = cv2.imread(os.path.join(pic_dir,pic_name),0)

        res = net.query(im, 50, retrieval_model)
        query_feature = net.extractFeature(im)
        gallery_features = features[res[0]]
        indexs, confidence = net.secondQuery(query_feature, gallery_features)
        print(res)

if __name__=="__main__":
    buildRetrievalDatabase()
    # query()

