import os
import cv2
from . import darknet

class IObjZoneYOLOV3Detect:
    def __init__(self, cfg_file, weight_file, class_num, gpu_id=0):
        self.__class_num = class_num
        darknet.set_gpu(gpu_id)
        self.__detector = darknet.load_net(bytes(cfg_file, encoding="utf8"), bytes(weight_file, encoding="utf8"), 1)

    @staticmethod
    def boxConvert(b, w, h):
        x1 = b[0] - b[2]/2
        y1 = b[1] - b[3]/2
        x2 = x1 + b[2]
        y2 = y1 + b[3]

        x1 = 0 if x1<0 else int(x1)
        y1 = 0 if y1<0 else int(y1)
        x2 = w if x2>w else int(x2)
        y2 = h if y2>h else int(y2)

        return (x1, y1, x2, y2)

    def detect(self, image_path, thresh = 0.7):
        result = darknet.detect(self.__detector, self.__class_num, bytes(image_path, encoding="utf8"), thresh)
        r = []
        h, w = cv2.imread(image_path, cv2.IMREAD_UNCHANGED).shape[:2]
        for tmp in result:
            temp = {}

            temp["zone"] = self.boxConvert(tmp[2], w, h)
            temp["cls"] = tmp[0]
            temp["score"] = tmp[1]
            r.append(temp)
        return r

def addRectangle(im,boxes):
    font = cv2.FONT_HERSHEY_SIMPLEX
    for box in boxes:
        zone = box["zone"]
        cv2.rectangle(im,(zone[0],zone[1]),(zone[2],zone[3]),(0,0,255),2)
        cv2.putText(im, "%s:%.2f"%(box["cls"],box["score"]),(zone[0],zone[1]),font,1,(0,255,0))


def run():
    cfg_file = "./model/yolov3.cfg"
    weight_file = "./model/yolov3.weights"
    name_file = "./model/yolov3.names"
    picdir = "./testimage/"
    pic_save_dir = "./result/"

    names = [name.strip() for name in open(name_file).readlines()]
    class_num = len(names)

    detector = IObjZoneYOLOV3Detect(cfg_file, weight_file, class_num)

    if not os.path.exists(pic_save_dir):
        os.makedirs(pic_save_dir)

    pic_names = os.listdir(picdir)

    for picname in pic_names:
        print(picname)
        if not picname.endswith(".jpg"):
            continue

        boxes = detector.detect(picdir+picname)
        im = cv2.imread(picdir+picname)
        addRectangle(im, boxes)
        cv2.imwrite(pic_save_dir+picname, im)

        print(boxes)

if __name__=="__main__":
    run()


