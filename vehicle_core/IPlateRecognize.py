from .IPlateChar import IPlateChar
from .IPlateZoneDetect import IPlateZoneDetect
from .IPlateClassifier import IPlateClassifier
import numpy as np
import os
import cv2
from PIL import Image

class IPlateRecognize:
    def __init__(self, model_dir, gpu_id=0):
        model_dir = model_dir + "/" if not model_dir.endswith("/") else model_dir
        model_path = model_dir + "vehicle/plate.pth"
        self.__net = IPlateZoneDetect(model_path, gpu_id)

        model_path = model_dir + "vehicle/platechar.pth"
        self.__net_char = IPlateChar(model_path, gpu_id)

        model_path = model_dir + "vehicle/platetype.pth"
        self.__net_type = IPlateClassifier(model_path, gpu_id)

    def recognize(self, img_bgr: np.array, confidence=0.3):
        # h, w = img_bgr.shape[:-1]
        # mid_h = int(h/2)
        # img_bgr = img_bgr[mid_h:]
        boxes = self.__net.detect(img_bgr, confidence)
        if not len(boxes):
            return []

        box = boxes[0]
        plate = IPlateZoneDetect.getPlate(img_bgr, box)
        plate_type = self.__net_type.classify(plate)
        if plate_type in ["whitejd2", "whitewj2", "yellow2"]:
            plate = self.cutImageDouble2Single(plate)
        license = self.__net_char.recognize(plate)

        # cv2.imshow("plate1", plate)
        box.append(license)
        box.append(plate_type)

        return box


    @staticmethod
    def cutImageDouble2Single(image):
        image = Image.fromarray(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
        imagefile = []
        width = int(image.size[0])
        height = int(image.size[1])
        img1 = image.crop((0, 0, width, int(height * 0.42)))
        img2 = image.crop((0, int(height * 0.42), width, height))

        img1resize = img1.resize((img2.size[0], img2.size[1]), Image.ANTIALIAS)

        img3 = img1resize.crop((25, 0, img1resize.size[0] - 25, img1resize.size[1]))
        img4 = img2.crop((3, 0, img2.size[0] - 3, img2.size[1]))
        imagefile.append(img3)
        imagefile.append(img4)

        TARGET_WIDTH = img3.size[0] + img4.size[0]
        target = Image.new('RGB', (TARGET_WIDTH, img4.size[1]))

        target.paste(img3, (0, 0, img3.size[0], img4.size[1]))
        target.paste(img4, (img3.size[0], 0, TARGET_WIDTH, img4.size[1]))

        target = cv2.cvtColor(np.asarray(target), cv2.COLOR_RGB2BGR)

        return target

if __name__=="__main__":
    model_dir = "/home/zqp/gitlab/models"
    detector = IPlateRecognize(model_dir)

    pic_dir = "/home/zqp/testpic/vehicle/"
    for pic_name in os.listdir(pic_dir):
        img = cv2.imread(pic_dir+pic_name)
        # img = cv2.resize(img, (640, 640), interpolation=cv2.INTER_CUBIC)
        result = detector.recognize(img)
        plate = IPlateZoneDetect.getPlate(img, result)

        print(result)
        # cv2.imshow("im", img)
        # cv2.imshow("plate", plate)
        # cv2.waitKey(0)

