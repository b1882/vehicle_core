import torch
import numpy as np
from .data import cfg_mnet
from .layers.functions.prior_box import PriorBox
from .utils.nms.py_cpu_nms import py_cpu_nms
import cv2
from .models.retina import Retina
from .utils.box_utils import decode, decode_landm
import os

class IPlateZoneDetect:
    def __init__(self, model_path, gpu_id=0):
        self.cfg = cfg_mnet

        self.device = torch.device("cuda:%s"%gpu_id if torch.cuda.is_available() else "cpu")
        pretrained_dict = torch.load(model_path, map_location=lambda storage, loc: storage.cuda(self.device))

        if "state_dict" in pretrained_dict.keys():
            pretrained_dict = self.remove_prefix(pretrained_dict['state_dict'], 'module.')
        else:
            pretrained_dict = self.remove_prefix(pretrained_dict, 'module.')

        torch.set_grad_enabled(False)
        self.net = Retina(cfg=self.cfg, phase='test')
        self.check_keys(self.net, pretrained_dict)
        self.net.load_state_dict(pretrained_dict, strict=False)
        self.net = self.net.to(self.device)
        self.net.eval()

        self.top_k = 100
        self.nms_threshold = 0.4
        self.vis_thres = 0.1

    @staticmethod
    def remove_prefix(state_dict, prefix):
        f = lambda x: x.split(prefix, 1)[-1] if x.startswith(prefix) else x
        return {f(key): value for key, value in state_dict.items()}

    @staticmethod
    def check_keys(model, pretrained_state_dict):
        ckpt_keys = set(pretrained_state_dict.keys())
        model_keys = set(model.state_dict().keys())
        used_pretrained_keys = model_keys & ckpt_keys
        unused_pretrained_keys = ckpt_keys - model_keys
        missing_keys = model_keys - ckpt_keys
        print('Missing keys:{}'.format(len(missing_keys)))
        print('Unused checkpoint keys:{}'.format(len(unused_pretrained_keys)))
        print('Used keys:{}'.format(len(used_pretrained_keys)))
        assert len(used_pretrained_keys) > 0, 'load NONE from pretrained checkpoint'
        return True

    @staticmethod
    def getPlate(img, box):
        pt0 = box[0]
        pt1 = box[1]
        roi = img[pt0[1]:pt1[1], pt0[0]:pt1[0]]
        x0, y0 = box[2][0] - pt0[0], box[2][1] - pt0[1]
        x1, y1 = box[3][0] - pt0[0], box[3][1] - pt0[1]
        x2, y2 = box[4][0] - pt0[0], box[4][1] - pt0[1]
        x3, y3 = box[5][0] - pt0[0], box[5][1] - pt0[1]

        points1 = np.float32([[x0, y0], [x1, y1], [x2, y2], [x3, y3]])
        points2 = np.float32([[0, 0], [120, 0], [0, 40], [120, 40]])

        # 计算得到转换矩阵
        M = cv2.getPerspectiveTransform(points1, points2)

        # 实现透视变换转换 94 24
        im_plate = cv2.warpPerspective(roi, M, (120, 40))

        return im_plate

    @staticmethod
    def addRectangle(img_bgr, boxes):
        for box in boxes:
            cv2.circle(img_bgr, box[2], 1, (0, 0, 255), 4)
            cv2.circle(img_bgr, box[3], 1, (0, 255, 255), 4)
            cv2.circle(img_bgr, box[4], 1, (0, 255, 0), 4)
            cv2.circle(img_bgr, box[5], 1, (255, 0, 0), 4)

        return img_bgr

    def detect(self, img_bgr, threshold=0.3):
        h, w = img_bgr.shape[:2]
        img_raw = cv2.resize(img_bgr, (640, 640), interpolation=cv2.INTER_CUBIC)
        fh, fw = float(h)/640, float(w)/640

        img = np.float32(img_raw)

        im_height, im_width = img.shape[:2]
        scale = torch.Tensor([img.shape[1], img.shape[0], img.shape[1], img.shape[0]])
        img -= (104, 117, 123)
        img = img.transpose(2, 0, 1)
        img = torch.from_numpy(img).unsqueeze(0)
        img = img.to(self.device)
        scale = scale.to(self.device)

        loc, conf, landms = self.net(img)  # forward pass

        priorbox = PriorBox(self.cfg, image_size=(im_height, im_width))
        priors = priorbox.forward()
        priors = priors.to(self.device)
        prior_data = priors.data
        boxes = decode(loc.data.squeeze(0), prior_data, self.cfg['variance'])
        boxes = boxes * scale
        boxes = boxes.cpu().numpy()

        scores = conf.squeeze(0).data.cpu().numpy()[:, 1]

        landms = decode_landm(landms.data.squeeze(0), prior_data, self.cfg['variance'])
        scale1 = torch.Tensor([img.shape[3], img.shape[2], img.shape[3], img.shape[2],
                               img.shape[3], img.shape[2],
                               img.shape[3], img.shape[2]])
        scale1 = scale1.to(self.device)
        landms = landms * scale1
        landms = landms.cpu().numpy()

        # ignore low scores
        inds = np.where(scores > threshold)[0]
        boxes = boxes[inds]
        landms = landms[inds]
        scores = scores[inds]

        # keep top-K before NMS
        order = scores.argsort()[::-1][:self.top_k]
        boxes = boxes[order]
        landms = landms[order]
        scores = scores[order]

        # do NMS
        dets = np.hstack((boxes, scores[:, np.newaxis])).astype(np.float32, copy=False)
        keep = py_cpu_nms(dets, self.nms_threshold)
        # keep = nms(dets, args.nms_threshold,force_cpu=args.cpu)
        dets = dets[keep, :]
        landms = landms[keep]

        # keep top-K faster NMS
        dets = dets[:self.top_k, :]
        landms = landms[:self.top_k, :]

        dets = np.concatenate((dets, landms), axis=1)
        boxes = []
        for b in dets:
            if b[4] < self.vis_thres:
                continue
            score = b[4]

            x1, y1, x2, y2 = b[0], b[1], b[2], b[3]
            if x1 < 0: x1 = 0
            if y1 < 0: y1 = 0
            if x2 < 0: x2 = 0
            if y2 < 0: y2 = 0
            points = [[x1, y1], [x2, y2], [b[9], b[10]], [b[11], b[12]], [b[7], b[8]], [b[5], b[6]]]
            box = []
            for p in points:
                box.append((int(p[0]*fw), int(p[1]*fh)))
            box.append(score)
            boxes.append(box)

        if len(boxes):
            sorted(boxes, key=lambda box : box[-1], reverse=True)
        return boxes

if __name__ == '__main__':
    pic_dir = "/home/zqp/testpic/vehicle/"
    model_dir = "/home/zqp/gitlab/models/"
    detector = IPlateZoneDetect(model_dir)

    for image_name in os.listdir(pic_dir):
        image_path = pic_dir + image_name
        print(image_path)
        img = cv2.imread(image_path, cv2.IMREAD_COLOR)
        img = cv2.resize(img, (640, 640), interpolation=cv2.INTER_CUBIC)
        boxes = detector.detect(img)

        im_tmp = img.copy()
        detector.addRectangle(im_tmp, boxes)
        cv2.imshow("im", im_tmp)

        detector.addRectangle(im_tmp, boxes)
        for box in boxes:
            print(box[-1])
            plate = detector.getPlate(img, box)
            cv2.imshow("plate", plate)
            cv2.waitKey(0)





