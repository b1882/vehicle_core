import numpy as np
import os
import cv2
import torch
from .mobileNetV3 import MobileNetV3
from torchvision import transforms
from PIL import Image
import time

class IPlateClassifier:
    def __init__(self, model_path: str, gpu_id=0):
        self.__label = ["blue", "green", "whitejc1", "whitejd1", "whitejd2", "whitewj1", "whitewj2", "yellow1", "yellow2"]
        self.__device = torch.device("cuda:%s" % gpu_id if torch.cuda.is_available() else "cpu")

        net = MobileNetV3(num_classes=len(self.__label), multiplier=0.25)
        net.load_state_dict(torch.load(model_path, map_location=self.__device)["state_dict"])

        self.__net = net.to(self.__device).eval()
        self.__transforms = transforms.Compose([
            transforms.Resize((224,140)),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])

    def classify(self, img_bgr: np.array):
        img_ = Image.fromarray(cv2.cvtColor(img_bgr, cv2.COLOR_BGR2RGB))

        size = img_.size
        box = [4, 4, size[0] - 4, size[1] - 4]
        img_crop = img_.crop(box)

        input = self.__transforms(img_crop)
        img = input.unsqueeze(0).to(self.__device)

        with torch.no_grad():
            output = self.__net(img)
            pred = torch.argmax(output, dim=1)

        return self.__label[pred]


if __name__=="__main__":
    model_path = "./model_best.pth"
    pic_dir = "/home/zqp/testpic/plate/"

    classifier = IPlateClassifier(model_path)

    for pic_name in os.listdir(pic_dir):
        img = cv2.imread(pic_dir+pic_name)
        start = time.time()
        result = classifier.classify(img)
        print("cost time: ", (time.time()-start)*1000)
        print(result)

        cv2.imshow("img", img)
        cv2.waitKey(0)

