#coding=utf-8
import os
import shutil
import cv2
import numpy as np
import math
import pickle
import time
from . import ResNet
from torchvision import transforms
import torch
from PIL import Image


class IVehicleHeadClassifier():
    def __init__(self,modelDir,gpu_id=0):
        modelDir = modelDir + '/' if not modelDir.endswith('/') else modelDir
        labelfile = modelDir+'vehicleTypeHead/name.txt'
        assert(labelfile)
        self.__label = [line.strip() for line in open(labelfile).readlines()]

        self.__device = torch.device("cuda:%s" % gpu_id if torch.cuda.is_available() else "cpu")
        self.__net = ResNet.ResNet101(len(self.__label)).resnet101model()
        self.__net.to(self.__device)

        model_path = modelDir+"vehicleTypeHead/vehicleHead.pth"
        assert(model_path)
        self.__net.load_state_dict(torch.load(model_path, map_location=self.__device))
        self.__net.eval()

        self.__transforms = transforms.Compose([
            transforms.Resize((224,224)),
            transforms.ToTensor()
            ])


    def classify(self,img_bgr):
        img_ = Image.fromarray(cv2.cvtColor(img_bgr, cv2.COLOR_BGR2RGB))

        input = self.__transforms(img_)
        img = input.unsqueeze(0).to(self.__device)

        with torch.no_grad():
            output, feature = self.__net(img)
            output = torch.softmax(output, dim=1)
            pred = torch.argmax(output, dim=1)

            feature = feature.cpu().numpy().flatten()
            feature = feature / np.sqrt(feature.dot(feature))
        return {"category":self.__label[pred], "score":output.tolist()[0][pred], "feature": feature}

    def extractFeature(self, img_bgr, *args):
        result = self.classify(img_bgr)

        return result["feature"]

def run():
    modelDir = r'/home/zqp/gitlab/models/'
    classifier = IVehicleHeadClassifier(modelDir,0)

    picDir = r'/home/zqp/testpic/vehicle1/'
    for picName in os.listdir(picDir):
        im = cv2.imread(picDir+picName)
        result = classifier.classify(im)
        print (result['category'],"****************",result['score'])
        cv2.imshow('im',im)
        if cv2.waitKey(0)==27:
            break

if __name__ == '__main__':
    run()

