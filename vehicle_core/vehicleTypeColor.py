import numpy as np
import os
import cv2
import torch
from .mobileNetV3 import MobileNetV3
from torchvision import transforms
from PIL import Image
import time

class IVehicleColorClassifier:
    def __init__(self, model_dir: str, gpu_id=0):
        # self.__label = ["orange","chengse","grey","white","mise","purple","red","green","blue","brown","gloden","yellow","black"]
        self.__label = ["橘色","橙色","灰色","白色","米色","紫色","红色","绿色","蓝色","褐色","金色","黄色","黑色"]
        self.__device = torch.device("cuda:%s" % gpu_id if torch.cuda.is_available() else "cpu")

        model_dir = model_dir + '/' if not model_dir.endswith('/') else model_dir
        model_path = model_dir+"vehicle/vehiclecolor.pth"
        net = MobileNetV3(num_classes=len(self.__label), multiplier=0.8)
        state_dict = torch.load(model_path, map_location=self.__device)["state_dict"]
        net.load_state_dict(state_dict)

        self.__net = net.to(self.__device).eval()
        self.__transforms = transforms.Compose([
            transforms.Resize((224,140)),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])

    def classify(self, img_bgr: np.array):
        img_ = Image.fromarray(cv2.cvtColor(img_bgr, cv2.COLOR_BGR2RGB))

        input = self.__transforms(img_)
        img = input.unsqueeze(0).to(self.__device)

        with torch.no_grad():
            output = self.__net(img)
            output = torch.softmax(output, dim=1)
            pred = torch.argmax(output, dim=1)

        return {"category":self.__label[pred], "score":output.tolist()[0][pred]}


if __name__=="__main__":
    model_dir = "/root/models/gantry"
    pic_dir = "/root/testpic/"

    classifier = IVehicleColorClassifier(model_dir)

    for pic_name in os.listdir(pic_dir):
        img = cv2.imread(pic_dir+pic_name)
        start = time.time()
        result = classifier.classify(img)
        print("cost time: ", (time.time()-start)*1000)
        print(result)

        #cv2.imshow("img", img)
        #cv2.waitKey(0)

