import os
import cv2
from .LPRNet import LPRNet
import torch
import numpy as np

CHARS = ['京', '沪', '津', '渝', '冀', '晋', '蒙', '辽', '吉', '黑',
         '苏', '浙', '皖', '闽', '赣', '鲁', '豫', '鄂', '湘', '粤',
         '桂', '琼', '川', '贵', '云', '藏', '陕', '甘', '青', '宁',
         '新', '警', '挂', '学',
         '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
         'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K',
         'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
         'W', 'X', 'Y', 'Z', 'I', 'O', '-'
         ]

class IPlateChar:
    def __init__(self, model_path, gpu_id=0):
        assert (os.path.exists(model_path))
        self.__img_size = (94, 24)
        self.__lpr_max_len = 8
        self.__class_num = len(CHARS)
        self.__net = LPRNet(self.__class_num, 0)
        self.__device = torch.device("cuda:%s"%gpu_id if torch.cuda.is_available() else "cpu")
        self.__net.to(self.__device)

        self.__net.load_state_dict(torch.load(model_path, map_location=self.__device))
        self.__net.eval()

    def recognize(self, img: np.array):
        img = cv2.resize(img, self.__img_size)
        img = (img.astype(np.float32) - 127.5)/128
        img = np.transpose(img, [2, 0, 1])
        img = torch.tensor(img).unsqueeze(0).to(self.__device)

        self.__net.eval()
        preb = self.__net(img).cpu().detach().numpy()[0]
        preb = np.argmax(preb, axis=0)

        result = ""
        prev_c = len(CHARS) - 1
        for c in preb:
            if c == len(CHARS) - 1:
                prev_c = c
                continue

            if c != prev_c:
                result += CHARS[c]
                prev_c = c

        return result


if __name__=="__main__":
    model_path = "/home/zqp/gitlab/models/vehicle/platechar.pth"
    detector = IPlateChar(model_path)

    pic_dir = "./testplateimage/"
    for pic_name in os.listdir(pic_dir):
        img = cv2.imread(pic_dir + pic_name)
        result = detector.recognize(img)

        print(result)
        cv2.imshow("im", img)
        cv2.waitKey(0)
